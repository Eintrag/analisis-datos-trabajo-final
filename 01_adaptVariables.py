import random

path = "datos/"
maxTotalLines = float("inf")

def processLine(line):
	fields = line.split("|")
	fields[80] = fields[80][1:]
	fields[82] = fields[82][1:]
	return ",".join(fields)
	
def sampleFromPopulation(filename):
	wroteLines = 0
	sample = open(path + "adapted_" + filename, "w")
	with open(path + filename, "r") as population:
		sample.write(population.readline().replace("|", ",")) #always include first line with the headers
		for line in population:
			if(wroteLines > maxTotalLines):
				break
			sample.write(processLine(line))	
			wroteLines += 1
	population.close()
	sample.close()
	
sampleFromPopulation("DATATHON_secuencial.csv")