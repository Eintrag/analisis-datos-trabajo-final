import random

path = "datos/"
maxTotalLines = float("inf")
municipioEntries = {}
maxSameMunicipio = 300

idEntries = {}
maxSameId = 20

def increaseOrSetCount(field, entriesCount):
	try:
		entriesCount[field] = entriesCount[field] + 1
	except:
		entriesCount[field] = 1

def canIncreaseCount(indicator, entriesCount, maxEntries):
	try:
		return entriesCount[indicator] < maxEntries
	except:
		return True

def shouldIncludeFields(fields):
	if(canIncreaseCount(municipioFromFields(fields), municipioEntries, maxSameMunicipio)):
		if(canIncreaseCount(idFromFields(fields), idEntries, maxSameId)):
			increaseOrSetCount(municipioFromFields(fields), municipioEntries)
			increaseOrSetCount(idFromFields(fields), idEntries)
			return True
	else: 
		return False
	
def municipioFromFields(fields):
	return fields[76]

def idFromFields(fields):
	return fields[79]

def writeLine(fields, destination):
	if(shouldIncludeFields(fields)):
		destination.write(",".join(fields))
		return 1
	else: 
		return 0
	
def sampleFromPopulation(filename):
	municipioEntries.clear()
	idEntries.clear()
	wroteLines = 0
	sample = open(path + "sample_" + filename, "w")
	with open(path + filename, "r") as population:
		sample.write(population.readline().replace("|", ",")) #always include first line with the headers
		for line in population:
			fields = line.split(",")
			if(wroteLines > maxTotalLines):
				break
			wroteLines += writeLine(fields, sample)
			
	population.close()
	sample.close()

	
sampleFromPopulation("winter14.csv")
sampleFromPopulation("summer15.csv")